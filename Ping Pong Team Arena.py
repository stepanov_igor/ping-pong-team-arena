#!/usr/bin/python3


#     This file is part of Ping Pong Team Arena.

#     Ping Pong Team Arena is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     Ping Pong Team Arena is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with Ping Pong Team Arena.  If not, see <https://www.gnu.org/licenses/>.


import importlib
import signal
import sys
import json
import time
import numpy as np
from Paths import Paths
from colorama import Fore
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtCore import Qt, QProcess, QTimer
from PyQt5.QtGui import QPainter, QIcon
from PyQt5.QtOpenGL import QGLWidget, QGLFormat, QGL


class Game(QGLWidget):
    def __init__(self, refreshRate):
        super().__init__(QGLFormat(QGL.DoubleBuffer | QGL.SampleBuffers))
        self.arenaWidth = self.frameGeometry().width()
        self.arenaHeight = self.frameGeometry().height()
        self.lastTime = time.time()
        self.setFocusPolicy(Qt.StrongFocus)
        self.timer = QTimer()
        self.timer.timeout.connect(self.timeout)
        self.timer.setInterval(int(1000 / refreshRate / 2))
        self.keyList = set()
        self.nowTime = time.time()
        self.game = importlib.import_module("Game", package=None).Game(self)
        self.timer.start()
        self.context().format().setSwapInterval(4)
        self.setAutoBufferSwap(False)

    def keyPressEvent(self, event):
        astr = event.nativeScanCode()
        self.keyList.add(astr)

    def keyReleaseEvent(self, event):
        self.game.keys(self.keyList)
        try:
            self.keyList.remove(event.nativeScanCode())
        except:
            pass

    def resizeEvent(self, event):
        self.arenaWidth = self.frameGeometry().width()
        self.arenaHeight = self.frameGeometry().height()
        self.arenaPercentX = self.arenaWidth / 500
        self.arenaPercentY = self.arenaHeight / 200
        self.arenaPercent = min(self.arenaPercentX, self.arenaPercentY)
        self.IndentX = (self.arenaWidth - 500 * self.arenaPercent) / 2
        self.IndentY = self.arenaHeight - (self.arenaHeight - 200 * self.arenaPercent) / 2
        self.load()
        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        self.physics()
        painter.beginNativePainting()
        self.draw(painter)
        painter.endNativePainting()
        self.swapBuffers()
        
    def dotTransform(self, dot):
        x = self.arenaPercent * dot[0] + self.IndentX
        y = self.IndentY - self.arenaPercent * dot[1]
        return np.array([x, y])
    
    def sizeTransform(self, w):
        return w * self.arenaPercent

    def timeout(self):
        self.update()

    def load(self):
        self.game.load()

    def draw(self, painter):
        self.game.draw(painter)

    def physics(self):
        self.nowTime = time.time()
        self.deltaTime = self.nowTime - self.lastTime
        if self.deltaTime > 1 / 51:
            self.deltaTime = 1 / 51
        self.game.timeout(self.deltaTime)
        self.lastTime = self.nowTime

    def exit(self):
        app.exit()
        
    def restart(self):
        QProcess().startDetached(sys.executable, sys.argv)
        self.exit()
        

class App(QMainWindow):
    def __init__(self, refreshRate):
        super().__init__()
        self.paths = Paths()
        sys.stdout.write('\033[2K\033[1G')
        print(f"{Fore.GREEN}Reading configure file...{Fore.RESET}")
        with open("Ping Pong Team Arena.json", "r") as file:
            self.cfg = json.load(file)
        self.mode = self.paths.getParametr(self.cfg, "System parametrs", "Game mode", "Value")
        self.width = self.paths.getParametr(self.cfg, "System parametrs", "Window width", "Value")
        self.height = self.paths.getParametr(self.cfg, "System parametrs", "Window height", "Value")
        sys.stdout.write('\033[2K\033[1G')
        print(f"{Fore.GREEN}Configure file readed.{Fore.RESET}")
        self.game = Game(refreshRate)
        self.setWindowTitle("Ping Pong Team Arena")
        self.setWindowIcon(QIcon(self.paths.pathTransform("Ping Pong Team Arena.svg")))
        self.setCentralWidget(self.game)
        if str(self.mode) == "Fullscreen":
            self.showFullScreen()
        if str(self.mode) == "Windowed":
            self.setGeometry(0, 0, int(self.width), int(self.height))
            self.show()
            
    def sigintHandler(self, *args):
        self.game.exit()


app = QApplication(sys.argv)
screen = app.primaryScreen()
size = screen.size()
refreshRate = screen.refreshRate()
sys.stdout.write('\033[2K\033[1G')
print('FPS: %s' % refreshRate)
ex = App(refreshRate)
signal.signal(signal.SIGINT, ex.sigintHandler)
sys.exit(app.exec_())
