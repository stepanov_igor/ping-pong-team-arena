// This file is part of Ping Pong Team Arena.

// Ping Pong Team Arena is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Ping Pong Team Arena is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Ping Pong Team Arena.  If not, see <https://www.gnu.org/licenses/>.

#include <vorbis/vorbisfile.h>
#include <vorbis/codec.h>
#include <iostream>
#include <list>
#include <AL/al.h>
#include <AL/alc.h>
#include <sndfile.h>
#include <unistd.h>
#include "OpenALLib.hh"

OpenAL::OpenAL(){
    std::cout << "\033[2K\033[1G";
    std::cout << "\x1b[33;1mOpening OpenAL device...\x1b[0m\n";
    device = alcOpenDevice(0);
    context = alcCreateContext(device, 0);
    alcMakeContextCurrent(context);
    std::cout << "\033[2K\033[1G";
    std::cout << "\x1b[33;1mOpenAL device opened.\x1b[0m\n";
}

ALuint OpenAL::load(std::string path){
    SNDFILE *file;
    SF_INFO sfiin;
    short *audioData;
    sfiin.format = 0;
    file = sf_open (path.c_str(), SFM_READ, &sfiin);
    int len = sfiin.frames * sfiin.channels;
    audioData = (short*) malloc (( ~0x7FF & (len + 0x7FF)) * sizeof(short));
    sf_readf_short (file, audioData, sfiin.frames);
    sf_close(file);
    ALuint source;
    alGenSources(1, &source);
    alSource3f(source, AL_POSITION, 0, 0, 0);
    alSource3f(source, AL_VELOCITY, 0, 0, 0);
    alSourcei(source, AL_LOOPING, 0);
    ALuint buffer;
    alGenBuffers(1, &buffer);
    alBufferData(buffer, sfiin.channels == 2 ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16, audioData, len * sfiin.channels, sfiin.samplerate);
    alSourceQueueBuffers(source, 1, &buffer);
    sourceList.push_back (source);
    bufferList.push_back (buffer);
    return source;
}

void OpenAL::play(ALuint source, float volume, bool looping){
    if (looping == true){
        alSourcei(source, AL_LOOPING, AL_TRUE);
    }
    alSourcef(source, AL_GAIN, volume);
    alSourcePlay(source);
}

void OpenAL::pause(ALuint source){
    alSourcePause(source);
}

void OpenAL::stop(ALuint source){
    alSourceStop(source);
}

OpenAL::~OpenAL(){
    std::cout << "\033[2K\033[1G";
    std::cout << "\x1b[33;1mClosing OpenAL device...\x1b[0m\n";
    for (ALuint source: sourceList){
        alSourceStop(source);
        alSourcei(source, AL_BUFFER, 0);
        alDeleteSources(1, &source);
    }
    for (ALuint buffer: bufferList){
        alDeleteBuffers(1, &buffer);
    }
    alcDestroyContext(context);
    alcCloseDevice(device);
    std::cout << "\033[2K\033[1G";
    std::cout << "\x1b[33;1mOpenAL device closed.\x1b[0m\n";
}
