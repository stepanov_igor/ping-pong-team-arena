#!/usr/bin/python3


#     This file is part of Ping Pong Team Arena.

#     Ping Pong Team Arena is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     Ping Pong Team Arena is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with Ping Pong Team Arena.  If not, see <https://www.gnu.org/licenses/>.


import os


class Paths:
    def __init__(self):
        super().__init__()
        self.root, exe = os.path.split(__file__)
        self.windows = "\\"
        self.linux = "/"
        self.flag = bool()
        if self.linux in self.root:
            self.flag = True
        else:
            self.flag = False
        
    def pathTransform(self, path):
        if self.flag == True:
            return self.root + "/" + path
        else:
            transformedPath = path.replace("/", "\\")
            return self.root + self.windows + transformedPath
    
    def getParametr(self, cfg, tag, parametrName, parametrType):
        for parametr in cfg[tag]:
            if parametr["Name"] == parametrName:
                return parametr[parametrType]