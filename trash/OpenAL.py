#!/usr/bin/python3


#     This file is part of Ping Pong Team Arena.

#     Ping Pong Team Arena is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     Ping Pong Team Arena is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with Ping Pong Team Arena.  If not, see <https://www.gnu.org/licenses/>.


import openal
import pyogg
import ctypes
import ctypes.util
import colorama
import sys


class OpenAL:
    def __init__(self):
        super().__init__()
        sys.stdout.write('\033[2K\033[1G')
        print('\x1b[2K' + f"{colorama.Fore.YELLOW}Opening OpenAL capture device...{colorama.Fore.RESET}")
        self.bufferList = list()
        self.sourceList = list()
        self.device = openal.alcOpenDevice(None)
        self.context = openal.alcCreateContext(self.device, None)
        openal.alcMakeContextCurrent(self.context)
        sys.stdout.write('\033[2K\033[1G')
        print('\x1b[2K' + f"{colorama.Fore.YELLOW}OpenAL capture device opened.{colorama.Fore.RESET}")
        
    def load(self, path):
        file = pyogg.VorbisFile(path)
        audioData = file.as_array()
        source = openal.ALuint()
        openal.alGenSources(1, source)
        openal.alSource3f(source, openal.AL_POSITION, 0, 0, 0)
        openal.alSource3f(source, openal.AL_VELOCITY, 0, 0, 0)
        openal.alSourcei(source, openal.AL_LOOPING, 0)
        buffer = openal.ALuint()
        openal.alGenBuffers(1, buffer)
        openal.alBufferData(buffer, openal.AL_FORMAT_STEREO16 if file.channels == 2 else openal.AL_FORMAT_MONO16, audioData.ctypes.data_as(ctypes.c_void_p), len(file.buffer), file.frequency)
        openal.alSourceQueueBuffers(source, 1, buffer)
        self.sourceList.append(source)
        self.bufferList.append(buffer)
        return source

    def play(self, source, volume, looping):
        if looping == True:
            openal.alSourcei(source, openal.AL_LOOPING, openal.AL_TRUE)
        openal.alSourcef(source, openal.AL_GAIN, volume * 0.01)
        openal.alSourcePlay(source)
        
    def pause(self, source):
        openal.alSourcePause(source)
        
    def stop(self, source):
        openal.alSourceStop(source)

    def __del__(self):
        sys.stdout.write('\033[2K\033[1G')
        print(f"{colorama.Fore.YELLOW}Closing OpenAL capture device...{colorama.Fore.RESET}")
        for source in self.sourceList:
            openal.alSourceStop(source)
            openal.alSourcei(source, openal.AL_BUFFER, 0)
            openal.alDeleteSources(1, source)
        for buffer in self.bufferList:
            openal.alDeleteBuffers(1, buffer)
        openal.alcDestroyContext(self.context)
        openal.alcCloseDevice(self.device)
        sys.stdout.write('\033[2K\033[1G')
        print(f"{colorama.Fore.YELLOW}OpenAL capture device closed.{colorama.Fore.RESET}")
