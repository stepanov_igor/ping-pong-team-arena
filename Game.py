#!/usr/bin/python3


#     This file is part of Ping Pong Team Arena.

#     Ping Pong Team Arena is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     Ping Pong Team Arena is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with Ping Pong Team Arena.  If not, see <https://www.gnu.org/licenses/>.


import os, sys
import json
import random
import time
import numpy as np
from PyQt5.QtGui import QFont, QTransform, QImage, QPainter, QColor, QFontMetrics
from PyQt5.QtCore import QRect
from colorama import Fore
from Paths import Paths
from OpenAL import OpenAL
from Engine import Physics, Wall, Ball, Texture
from PyQt5.QtSvg import QSvgRenderer


class Game:
    def __init__(self, penguin):
        super().__init__()
        self.penguin = penguin
        self.tux = OpenAL()
        self.phys = Physics(self)
        self.paths = Paths()
        self.drawFPS = False
        self.drawTime = False
        self.drawFrags = False
        self.drawContours = False
        self.drawTextures = False
        self.playMusic = False
        self.playSounds = False
        self.run = False
        self.keySet = set()
        self.transform = QTransform()
        self.configure()
        self.menu = Menu(self)
        self.ping = PingPong(self)
        self.focus = self.menu
        self.changeState()
        
    def configure(self):
        with open("Ping Pong Team Arena.json", "r") as file:
            self.cfg = json.load(file)
        self.drawFPS = self.paths.getParametr(self.cfg, "Game parametrs", "Draw FPS", "Value")
        self.drawTime = self.paths.getParametr(self.cfg, "Game parametrs", "Draw Time", "Value")
        self.drawFrags = self.paths.getParametr(self.cfg, "Game parametrs", "Draw Frags", "Value")
        self.drawContours = self.paths.getParametr(self.cfg, "Game parametrs", "Draw Contours", "Value")
        self.drawTextures = self.paths.getParametr(self.cfg, "Game parametrs", "Draw Textures", "Value")
        self.playMusic = self.paths.getParametr(self.cfg, "Game parametrs", "Play Music", "Value")
        self.playSounds = self.paths.getParametr(self.cfg, "Game parametrs", "Play Sounds", "Value")

    def timeout(self, dt):
        self.focus.timeout(dt)
        if self.run == True:
            if len(self.focus.objects) > 0:
                self.dt = int(1 / dt)
                for o in self.focus.objects:
                    o.calc_vel_coord(dt)
                for j in range(0, len(self.focus.objects)):
                    for i in range(j + 1, len(self.focus.objects)):
                        o1 = self.focus.objects[j]
                        o2 = self.focus.objects[i]
                        if o2 is not o1:
                            self.phys.collide(o1, o2)
        
    def load(self):
        self.focus.load()
        if len(self.focus.objects) > 0:
            for o in self.focus.objects:
                o.load(self)

    def draw(self, painter):
        if len(self.focus.objects) > 0:
            for o in self.focus.objects:
                 o.draw(painter, self)
        self.focus.draw(painter)
    
    def keys(self, keys):
        self.keySet = keys
        self.focus.keys(self.keySet)
                    
    def hit(self, elem1, elem2, vel, center):
        self.focus.hit(elem1, elem2, vel, center)
        for group in self.focus.groups:
            if elem1 in group:
                for elem in group:
                    if elem != elem1:
                        elem.center += center
                        elem.vel += vel
    
    def changeState(self):
        if self.run == True:
            self.run = False
        else:
            self.run = True
                    
    def changeFocus(self):
        if self.focus == self.menu:
            self.focus = self.ping
            self.load()
            return
        if self.focus == self.ping:
            self.focus = self.menu
            self.focus.menuFocus = self.focus.GameMenu
            self.focus.num_now = 1
            self.focus.num_of_points = len(self.focus.menuFocus) - 1
            self.load()
            return
    
    def loadObject(self, path, skin):
        with open(path, "r") as file:
            e = json.load(file)
            elements = list()
            for o in e["Objects"]:
                if o["Type"] == "Ball":
                    element = Ball(radius = o["Radius"], vel = o["Vel"], accel = o["Accel"], center = o["Center"], mass = o["Mass"], id = o["Id"])
                if o["Type"] == "Wall":
                    element = Wall(center = o["Center"], norm = o["Norm"], len = o["Len"], mass = o["Mass"], id = o["Id"])
                elements.append(element)
            element = Texture(center = e["Skins"][skin]["Center"], textureSize = e["Skins"][skin]["Size"], texturePath = self.paths.pathTransform(e["Skins"][skin]["Texture"]), mass = o["Mass"], vel = e["Skins"][skin]["Vel"], id = e["Skins"][skin]["Id"])
            elements.append(element)
            return elements
             
    def drawText(self, painter, font, text, scale, x, y, color):
        painter.setFont(font)
        painter.setPen(color)
        w = painter.fontMetrics().width(text)
        h = painter.fontMetrics().height()
        scale_x = self.penguin.sizeTransform(scale * w) / w
        scale_y = self.penguin.sizeTransform(scale * h) / h
        rectangle = QRect(0, 0, w, h)    
        self.transform.reset()
        dot = self.penguin.dotTransform(np.array([x, y]))
        self.transform.translate(dot[0], dot[1])
        self.transform.scale(scale_x, scale_y)
        painter.setTransform(self.transform)
        painter.drawText(rectangle, 0, text)
        painter.resetTransform()
    
    def loadSvg(self, path, x, y):
        if self.drawTextures == True:
            w = self.penguin.sizeTransform(x)
            h = self.penguin.sizeTransform(y)
            image = QSvgRenderer(path)
            img = QImage(int(w), int(h), QImage.Format_ARGB32)
            img.fill(0x00000000)
            painter = QPainter(img)
            image.render(painter)
            return img
    
    def drawImage(self, dot, image, painter):
        if self.drawTextures == True:
            v = self.penguin.dotTransform(dot)
            painter.drawImage(int(v[0]), int(v[1]), image)
        
    def drawLine(self, v1, v2, painter):
        if self.drawContours == True:
            v1 = self.penguin.dotTransform(v1)
            v2 = self.penguin.dotTransform(v2)
            painter.drawLine(int(v1[0]), int(v1[1]), int(v2[0]), int(v2[1]))
        
    def drawEllipse(self, dot, x, y, painter):
        if self.drawContours == True:
            dot = self.penguin.dotTransform(dot)
            x = self.penguin.sizeTransform(x)
            y = self.penguin.sizeTransform(y)
            painter.drawEllipse(int(dot[0]), int(dot[1]), int(x), int(y))
            
            
class Player:
    def __init__(self, **kwargs):
        self.racquet = None
        self.stepSize = int()
        self.keyUp = int()
        self.keyDown = int()
        self.team = int()
        self.__dict__.update(kwargs)
        
    def count(self):
        if self.team == 0:
            sys.stdout.write('\033[2K\033[1G')
            print(f"{Fore.BLUE}Human joined to Pagans team.{Fore.RESET}")
        if self.team == 1:
            sys.stdout.write('\033[2K\033[1G')
            print(f"{Fore.RED}Human joined to Stroggs team.{Fore.RESET}")
        
    def run(self, tux):
        if self.keyUp in tux.keyList:
            tux.move(self.racquet, 1, self.stepSize)
        if self.keyDown in tux.keyList:
            tux.move(self.racquet, -1, self.stepSize)
            
            
class Bot:
    def __init__(self, **kwargs):
        self.racquet = None
        self.stepSize = int()
        self.matchType = int()
        self.team = int()
        self.__dict__.update(kwargs)
        self.targetBall = None
        
    def count(self):
        if self.team == 0:
            sys.stdout.write('\033[2K\033[1G')
            print(f"{Fore.BLUE}Bot joined to Pagans team.{Fore.RESET}")
        if self.team == 1:
            sys.stdout.write('\033[2K\033[1G')
            print(f"{Fore.RED}Bot joined to Stroggs team.{Fore.RESET}")
        
    def run(self, tux):
        dangers = list()
        balls = list()
        for elem in tux.groups:
            if elem[0].id == 2:
                balls.append(elem)
        for ball in balls:
            if self.team == 0 and ball[0].vel[0] < 0 or self.team == 1 and ball[0].vel[0] > 0:
                dangers.append(ball)
        if len(dangers) == 0:
            return
        if self.matchType == 0:
            velocity = list()
            for ball in dangers:
                velocity.append(abs(ball[0].vel[0]))
            self.targetBall = dangers[velocity.index(max(velocity))][0]
        if self.matchType == 1:
            return
        posBallY = self.targetBall.center[1]
        posBotY = self.racquet[1].center[1]
        if posBotY < posBallY:
            tux.move(self.racquet, 1, self.stepSize)
        if posBotY > posBallY:
            tux.move(self.racquet, -1, self.stepSize)
            
            
class PingPong:
     def __init__(self, tux):
        super().__init__()
        self.tux = tux
        self.paths = Paths()
        self.showFrags = False
        self.fragLimit = int()
        self.timeLimit = int()
        self.themeSound = None
        self.keyList = set()
        self.startTime = time.time()
        self.bots = list()
        self.players = list()
        self.blueScores = 0
        self.redScores = 0
        self.red = 1
        self.blue = 0
        self.bluePos = [[30.0, 100.0], [60.0, 100.0]]
        self.redPos = [[460.0, 100.0], [430.0, 100.0]]
        self.objects = list()
        self.groups = list()
        self.musics = list()
        self.sounds = list()
        self.music()
        self.hitSound = self.tux.tux.load(self.paths.pathTransform("sounds/sound_etr_tree_hit.ogg"))
        self.setMatchType("Duel")
        self.setFragLimit(50)
        self.setTimeLimit(15)
        self.addTable('game images/tables/table_classic.json', 0)
        self.addBall('game images/balls/ball_classic.json', 1)
        self.addBall('game images/balls/ball_classic.json', 0)
        self.addBot("game images/racquets/racquet_classic.json", 1, 50, self.red, 0)
        self.addBot('game images/racquets/racquet_classic.json', 0, 50, self.blue, 0)
        self.addPlayer('game images/racquets/racquet_classic.json', 1, 250, self.red, 33, 46, 1)
        self.addPlayer('game images/racquets/racquet_classic.json', 0, 250, self.blue, 24, 38, 1)

     def timeout(self, dt):
        for e in self.players:
            for o in e.racquet:
                    o.vel[1] = 0
        for e in self.bots:
            for o in e.racquet:
                    o.vel[1] = 0

        self.dt = int(1 / dt)
        self.overTime = str(int((self.timeLimit - (time.time() - self.startTime)) / 60)) + ":" + str(int((self.timeLimit - (time.time() - self.startTime)) % 60))
        if len(self.bots) != 0:
            for bot in self.bots:
                bot.run(self)
        if len(self.players) != 0:
            for player in self.players:
                player.run(self)
        
     def load(self):
        self.IconFPS = self.tux.loadSvg("game images/icons/FPS.svg", 12, 15)
        self.IconTime = self.tux.loadSvg("game images/icons/Time.svg", 12, 15)
        self.IconFrags = self.tux.loadSvg("game images/icons/Frags.svg", 12, 15)
        
     def draw(self, painter):
         if self.showFrags == True:
            painter.setFont(QFont("GOST type B", 12))
            w = painter.fontMetrics().width(str(self.blueScores)) * 2
            self.tux.drawText(painter, QFont("GOST type B", 12), str(self.blueScores), 2, 235 - w, 175, QColor(0, 0, 255, 255))
            w = painter.fontMetrics().width(str(self.redScores)) * 2
            self.tux.drawText(painter, QFont("GOST type B", 12), str(self.redScores), 2, 265, 175, QColor(255, 0, 0, 255))
         painter.setFont(QFont("GOST type B", 12))
         if self.tux.drawFPS == True:
            wf = painter.fontMetrics().width(str(self.dt))
            self.tux.drawImage(np.array([474.0 - wf, 192.5]), self.IconFPS, painter)
            self.tux.drawText(painter, QFont("GOST type B", 12), str(self.dt), 1, 490.0 - wf, 191.0, QColor(0, 0, 0, 255))
         if self.tux.drawTime == True:
            wt = painter.fontMetrics().width(str(self.overTime))
            self.tux.drawImage(np.array([224.0 - wt, 192.5]), self.IconTime, painter)
            self.tux.drawText(painter, QFont("GOST type B", 12), str(self.overTime), 1, 240.0 - wt, 191.0, QColor(0, 0, 0, 255))
         if self.tux.drawFrags == True:
            self.tux.drawImage(np.array([260.0, 192.5]), self.IconFrags, painter)
            wb = painter.fontMetrics().width(str(self.blueScores))
            wf = painter.fontMetrics().width(str(self.fragLimit))
            self.tux.drawText(painter, QFont("GOST type B", 12), str(self.blueScores), 1, 276.0, 191.0, QColor(0, 0, 255, 255))
            self.tux.drawText(painter, QFont("GOST type B", 12), str(self.fragLimit), 1, 272.0 + wb + 8.0, 191.0, QColor(0, 0, 0, 255))
            self.tux.drawText(painter, QFont("GOST type B", 12), str(self.redScores), 1, 272.0 + wb + wf + 12.0, 191.0, QColor(255, 0, 0, 255))
        
     def keys(self, keys):
        self.keyList = keys
        if 9 in keys:
            self.tux.changeFocus()
        if 23 in keys:
            if self.showFrags == False:
                self.showFrags = True
            else:
                self.showFrags = False
                
     def hit(self, elem1, elem2, vel, center):
        if elem1.id == 2 or elem2.id == 2:
            if self.tux.playSounds == True:
                self.tux.tux.play(self.hitSound, 50, False)
        if elem2.id == 0:
            self.redScores += 1
        if elem2.id == 1:
            self.blueScores += 1
                
     def move(self, racquet, step, stepSize):
        if step == 1:
            for o in racquet:
                o.vel[1] = stepSize
        if step == -1:
            for o in racquet:
                o.vel[1] = -stepSize
                
     def music(self):
        if self.tux.playMusic == True:
            self.musics = list()
            if self.themeSound != None:
                self.tux.tux.stop(self.themeSound)
            for music in os.listdir(self.paths.pathTransform("/sounds")):
                if music.startswith('music'):
                    self.musics.append(music)
            random_music = random.randint(0, len(self.musics) - 1)
            self.themeSound = self.tux.tux.load(self.paths.pathTransform('sounds/' + self.musics[random_music]))
            self.tux.tux.play(self.themeSound, 30, True)
                
     def count(self):
        if len(self.bots) != 0:
            for bot in self.bots:
                bot.count()
        if len(self.players) != 0:
            for player in self.players:
                player.count()
                
     def setMatchType(self, type):
        if type == "Duel":
            self.matchType = 0
        if type == "Team":
            self.matchType = 1
        
     def setFragLimit(self, number):
        self.fragLimit = number
        
     def setTimeLimit(self, minutes):
        self.timeLimit = 60 * minutes
        
     def addTable(self, path, skin):
        table = self.tux.loadObject(self.paths.pathTransform(path), skin)
        self.groups.append(table)
        for e in table:
            self.objects.append(e)
        return table 
        
     def addBall(self, path, skin):
        ball = self.tux.loadObject(self.paths.pathTransform(path), skin)
        x = random.randint(100, 400)
        y = random.randint(50, 150)
        for elem in ball:
            elem.center += np.array([x, y])
        self.count()
        self.groups.append(ball)
        for e in ball:
            self.objects.append(e)
        return ball

     def addBot(self, path, skin, stepSize, team, pos):
        bot = self.tux.loadObject(self.paths.pathTransform(path), skin)
        if team == 0:
            list = self.bluePos
        else:
            list = self.redPos
        for e in bot:
            e.center += list[pos]
        self.bots.append(Bot(racquet = bot, stepSize = stepSize, matchType = self.matchType, team = team))
        self.count()
        self.groups.append(bot)
        for e in bot:
            self.objects.append(e)
        return bot
        
     def addPlayer(self, path, skin, stepSize, team, key_up, key_down, pos):
        player = self.tux.loadObject(self.paths.pathTransform(path), skin)
        if team == 0:
            list = self.bluePos
        else:
            list = self.redPos
        for e in player:
            e.center += list[pos]
        self.players.append(Player(racquet = player, stepSize = stepSize, team = team, keyUp = key_up, keyDown = key_down))
        self.count()
        self.groups.append(player)
        for e in player:
            self.objects.append(e)
        return player
    
    
class MenuItem:  
    def __init__(self, text, scale, x, y, url, fun):
        super().__init__()
        self.parent = None
        self.text = text
        self.scale = scale
        self.x = x
        self.y = y
        self.url = url
        self.fun = fun
        

class Menu:
    def __init__(self, tux):
        super().__init__()
        self.tux = tux
        self.paths = Paths()
        self.menuThemeImage_path = str()
        self.menuThemeImage_object = None
        self.menuThemeSound_object = None
        self.menuCursorImage_path = str()
        self.menuCursorImage_object = None
        self.menuCursor_indent = int()
        self.menuCursorSound_object = None        
        self.menuFont = None
        self.objects = list()
        self.groups = list()
        self.musics = list()
        self.sounds = list()
        self.Menu = list()
        self.Network = list()
        self.Local = list()
        self.Demos = list()
        self.Settings = list()
        self.System = list()
        self.Game = list()
        self.GameMenu = list()
        with open("Ping Pong Team Arena.json", "r") as file:
            self.cfg = json.load(file)
        self.setFont('GOST type B', 12)
        self.setThemeImage('menu images/theme.svg')
        self.setThemeSound('sounds/music_etr_start.ogg')
        self.setCursorImage('menu images/cursor.svg')
        self.setCursorSound('sounds/sound_etr_pickup_1.ogg')
        self.setCursorIndent(20)
        
        self.addMenu(self.Menu, MenuItem('Ping Pong Team Arena', 2, 98, 196, None, None))
        self.addMenu(self.Menu, MenuItem('Network Battle', 1, 198.5, 153, self.Network, None))
        self.addMenu(self.Menu, MenuItem('Local Battle', 1, 209, 132, self.Local, None))
        self.addMenu(self.Menu, MenuItem('Demos', 1, 229, 111, self.Demos, None))
        self.addMenu(self.Menu, MenuItem('Settings', 1, 222, 90, self.Settings, None))
        self.addMenu(self.Menu, MenuItem('Restart', 1, 224.5, 69, None, self.tux.penguin.restart))
        self.addMenu(self.Menu, MenuItem('Exit', 1, 237.5, 48, None, self.tux.penguin.exit))
        
        self.addMenu(self.Settings, MenuItem('Settings', 2, 194, 196, None, None))
        self.addMenu(self.Settings, MenuItem('Game', 1, 233, 153, self.Game, None))
        self.addMenu(self.Settings, MenuItem('Player', 1, 228.5, 132, None, None))
        self.addMenu(self.Settings, MenuItem('Control', 1, 226, 111, None, None))
        self.addMenu(self.Settings, MenuItem('System', 1, 226, 90, self.System, None))
        self.addMenu(self.Settings, MenuItem('Back', 1, 234.5, 69, self.Menu, None))
        
        self.addMenu(self.System, MenuItem('System', 2, 202, 196, None, None))
        self.addMenu(self.System, MenuItem("Game mode: " + str(self.paths.getParametr(self.cfg, "System parametrs", "Game mode", "Value")), 1, 180, 153, None, None))
        self.addMenu(self.System, MenuItem("Window width: " + str(self.paths.getParametr(self.cfg, "System parametrs", "Window width", "Value")), 1, 180, 132, None, None))
        self.addMenu(self.System, MenuItem("Window height: " + str(self.paths.getParametr(self.cfg, "System parametrs", "Window height", "Value")), 1, 180, 111, None, None))
        self.addMenu(self.System, MenuItem('Back', 1, 27, 45, self.Settings, None))
        
        self.addMenu(self.Game, MenuItem('Game', 2, 202, 196, None, None))
        self.addMenu(self.Game, MenuItem("Draw FPS: " + str(self.paths.getParametr(self.cfg, "Game parametrs", "Draw FPS", "Value")), 1, 180, 153, None, None))
        self.addMenu(self.Game, MenuItem("Draw Time: " + str(self.paths.getParametr(self.cfg, "Game parametrs", "Draw Time", "Value")), 1, 180, 132, None, None))
        self.addMenu(self.Game, MenuItem("Draw Frags: " + str(self.paths.getParametr(self.cfg, "Game parametrs", "Draw Frags", "Value")), 1, 180, 111, None, None))
        self.addMenu(self.Game, MenuItem("Draw Contours: " + str(self.paths.getParametr(self.cfg, "Game parametrs", "Draw Contours", "Value")), 1, 180, 90, None, None))
        self.addMenu(self.Game, MenuItem("Draw Textures: " + str(self.paths.getParametr(self.cfg, "Game parametrs", "Draw Textures", "Value")), 1, 180, 69, None, None))
        self.addMenu(self.Game, MenuItem("Play Music: " + str(self.paths.getParametr(self.cfg, "Game parametrs", "Play Music", "Value")), 1, 180, 48, None, None))
        self.addMenu(self.Game, MenuItem("Play Sounds: " + str(self.paths.getParametr(self.cfg, "Game parametrs", "Play Sounds", "Value")), 1, 180, 27, None, None))
        self.addMenu(self.Game, MenuItem('Back', 1, 30, 45, self.Settings, None))
        
        self.addMenu(self.Network, MenuItem('Network Battle', 2, 147, 196, None, None))
        self.addMenu(self.Network, MenuItem('Back', 1, 30, 33, self.Menu, None))
        
        self.addMenu(self.Local, MenuItem('Local Battle', 2, 168, 196, None, None))
        self.addMenu(self.Local, MenuItem('Back', 1, 30, 33, self.Menu, None))
        self.addMenu(self.Local, MenuItem('Play', 1, 450, 33, None, self.tux.changeFocus))
        
        self.addMenu(self.Demos, MenuItem('Demos', 2, 208, 196, None, None))
        self.addMenu(self.Demos, MenuItem('Back', 1, 30, 33, self.Menu, None))
        
        self.addMenu(self.GameMenu, MenuItem("Game", 2, 180, 196, None, None))
        self.addMenu(self.GameMenu, MenuItem('Resume', 1, 450, 33, None, self.tux.changeFocus))
        self.addMenu(self.GameMenu, MenuItem('Local Battle', 1, 209, 132, self.Local, None))
        
        self.menuFocus = self.Menu
        self.num_now = 1
        self.num_of_points = len(self.menuFocus) - 1
        
    def addMenu(self, menu, item: MenuItem):
        menu.append(item)
        item.parent = menu

    def setThemeImage(self, path):
        self.menuThemeImage_path = self.paths.pathTransform(path)
        
    def setThemeSound(self, path):
        self.menuThemeSound_object = self.tux.tux.load(self.paths.pathTransform(path))
        self.tux.tux.play(self.menuThemeSound_object, 30, True)
        
    def setCursorImage(self, path):
        self.menuCursorImage_path = self.paths.pathTransform(path)
        
    def setCursorIndent(self, indent):
        self.menuCursor_indent = indent
        
    def setCursorSound(self, path):
        self.menuCursorSound_object = self.tux.tux.load(self.paths.pathTransform(path))
        
    def setFont(self, font, font_size):
        self.menuFont = QFont(font, font_size)
        
    def timeout(self, dt):
        return
    
    def hit(self, elem1, elem2, vel, center):
        return
        
    def load(self):
        self.menuThemeImage_object = self.tux.loadSvg(self.menuThemeImage_path, 500.0, 200.0)
        self.menuCursorImage_object = self.tux.loadSvg(self.menuCursorImage_path, QFontMetrics(self.menuFont).height() * 2, QFontMetrics(self.menuFont).height())
        
    def draw(self, painter): 
        self.tux.drawImage(np.array([0, 200]), self.menuThemeImage_object, painter)
        self.tux.drawImage(np.array([self.menuFocus[self.num_now].x - QFontMetrics(self.menuFont).height() - self.menuCursor_indent, self.menuFocus[self.num_now].y]), self.menuCursorImage_object, painter)
        for point in self.menuFocus:
            self.tux.drawText(painter, self.menuFont, point.text, point.scale, point.x, point.y, QColor(0, 0, 0, 255))

    def keys(self, keys):
        if 36 in keys and self.menuFocus[self.num_now].url is not None:
            self.menuFocus = self.menuFocus[self.num_now].url
            self.num_now = 1
            self.num_of_points = len(self.menuFocus) - 1
        if 36 in keys and self.menuFocus[self.num_now].fun is not None:
            self.menuFocus[self.num_now].fun()
        if 111 in keys:
            self.num_now -= 1
            if self.num_now < 1:
                self.num_now = self.num_of_points
        if 116 in keys:
            self.num_now += 1
            if self.num_now > self.num_of_points:
                self.num_now = 1
        self.tux.tux.play(self.menuCursorSound_object, 80, False)
