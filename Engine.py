#!/usr/bin/python3


#     This file is part of Ping Pong Team Arena.

#     Ping Pong Team Arena is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     Ping Pong Team Arena is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with Ping Pong Team Arena.  If not, see <https://www.gnu.org/licenses/>.


import numpy as np


class Obj:
    def __init__(self, **kwargs):
        self.center = np.array([0.0, 0.0])
        self.vel = np.array([0., 0.])
        self.accel = np.array([0.0, 0.0])
        self.mass = 1
        self.id = int()
        self.__dict__.update(kwargs)
        self.center = np.array(self.center)
        self.vel = np.array(self.vel)
        self.accel = np.array(self.accel)

    def calc_vel_coord(self, dt):
        self.vel += self.accel * dt
        self.center += self.vel * dt


class Ball(Obj):
    def __init__(self, **kwargs):
        self.radius = 1.
        self.accel = np.array([0.0, 0.0])
        Obj.__init__(self, **kwargs)
        self.mass *= self.radius**2
        
    def load(self, tux):
        return
        
    def draw(self, painter, tux):
        dot = np.array([self.center[0] - self.radius, self.center[1] + self.radius])
        tux.drawEllipse(dot, self.radius * 2.0, self.radius * 2.0, painter)


class Wall(Obj):
    def __init__(self, **kwargs):
        self.norm = np.array([0.0, 1.0])
        Obj.__init__(self, **kwargs)
        self.norm = np.array(self.norm)
        self.norm = self.norm / np.linalg.norm(self.norm)
        self.mass = 1e100
        self.accel = np.array([0.0, 0.0])
        
    def load(self, tux):
        return

    def draw(self, painter, tux):
        e = np.array([self.norm[1], - self.norm[0]])
        v1 = self.center - self.len * 0.5 * e
        v2 = self.center + self.len * 0.5 * e
        tux.drawLine(v1, v2, painter)


class Texture(Obj):
    def __init__(self, **kwargs):
        self.texturePath = str()
        self.textureObject = None
        self.textureSize = [0.0, 0.0]
        Obj.__init__(self, **kwargs)
        
    def load(self, tux):
        self.textureObject = tux.loadSvg(self.texturePath, self.textureSize[0], self.textureSize[1])

    def draw(self, painter, tux):
        tux.drawImage(self.center, self.textureObject, painter)


class Physics:
    def __init__(self, tux):
        self.tux = tux
        self.fmap = dict()
        self.fmap[Ball().__class__.__name__] = dict()
        self.fmap[Ball().__class__.__name__][Ball().__class__.__name__] = self.ball_ball
        self.fmap[Ball().__class__.__name__][Wall().__class__.__name__] = self.ball_wall

    def collide(self, obj1, obj2):
        if self.fmap.get(obj1.__class__.__name__) is None:
            obj1, obj2 = obj2, obj1
            if self.fmap.get(obj1.__class__.__name__) is None:
                return
        if self.fmap[obj1.__class__.__name__].get(obj2.__class__.__name__) is None:
            return
        else:
            self.fmap[obj1.__class__.__name__][obj2.__class__.__name__](obj1, obj2)

    def ball_ball(self, obj1: Ball, obj2: Ball):
        if obj1.mass < obj2.mass:
            (obj1, obj2) = (obj2, obj1)
        if np.linalg.norm(obj1.center - obj2.center) > (obj1.radius + obj2.radius):
            return
        else:
            v1 = obj1.vel
            v2 = obj2.vel
            x1 = obj1.center
            x2 = obj2.center
            dv = v2 - v1
            dx = x2 - x1
            m1 = obj1.mass
            m2 = obj2.mass
            ms = m1 + m2
            du = dx.dot(dv) * dx / dx.dot(dx)
            if obj1.mass < 1e10:
                u1 = v1 + 2.0 * m2 / ms * du
                u2 = v2 - 2.0 * m1 / ms * du
            else:
                u1 = v1
                u2 = v2 - 2.0 * du
                u2 = np.linalg.norm(v2) * u2 / np.linalg.norm(u2)
            if obj1.mass < 1e10:
                vel = obj1.vel
                obj1.vel = u1
                self.tux.hit(obj1, obj2, obj1.vel - vel, np.array([0.0, 0.0]))
            if obj2.mass < 1e10:
                vel = obj2.vel
                obj2.vel = u2
                self.tux.hit(obj2, obj1, obj2.vel - vel, np.array([0.0, 0.0]))
            d = np.sqrt((x2[0] - x1[0])**2.0 + (x2[1] - x1[1])**2.0)
            if d > obj1.radius + obj2.radius:
                return None
            if d < abs(obj1.radius - obj2.radius):
                return None
            if d == 0.0 and obj1.radius == obj2.radius:
                return None
            else:
                dist = (x2 - x1)
                dist_l = np.linalg.norm(dist)
                dist_n = dist / dist_l
                r1s = obj1.radius ** 2
                r2s = obj2.radius ** 2
                diff = (r2s - r1s) / (2 * dist_l)
                H1 = dist_l / 2 - diff
                H2 = dist_l / 2 + diff
                h1 = (H1 -obj1.radius) * dist_n
                h2 = - (H2 -obj2.radius) * dist_n
                if obj1.mass < 1e10:
                    obj1.center += h1
                    self.tux.hit(obj1, obj2, np.array([0.0, 0.0]), h1)
                if obj2.mass < 1e10:
                    obj2.center += h2
                    self.tux.hit(obj2, obj1, np.array([0.0, 0.0]), h2)

    def ball_wall(self, ball: Ball, wall: Wall):
        e = np.array([wall.norm[1], wall.norm[0]])
        d = ball.center - wall.center
        vel = ball.vel
        center = ball.center
        if np.abs(d.dot(e)) < wall.len * 0.5 and 0.0 < d.dot(wall.norm) < ball.radius:
            ball.vel = ball.vel - 2.0 * wall.norm * ball.vel.dot(wall.norm)
            ball.center = ball.center - wall.norm * ((ball.center - wall.center).dot(wall.norm) - ball.radius)
            self.tux.hit(ball, wall, ball.vel - vel, ball.center - center)
        else:
            p1 = wall.center + e * wall.len * 0.5
            p2 = wall.center - e * wall.len * 0.5
            d1 = ball.center - p1
            d2 = ball.center - p2
            r2 = ball.radius ** 2.0
            if d1.dot(d1) < r2:
                n = d1 / np.linalg.norm(d1)
                ball.vel = ball.vel - 2.0 * n * ball.vel.dot(n)
                ball.center = ball.center - n * ((ball.center - p1).dot(n) - ball.radius)
                self.tux.hit(ball, wall, ball.vel - vel, ball.center - center)
            elif d2.dot(d2) < r2:
                n = d2 / np.linalg.norm(d2)
                ball.vel = ball.vel - 2.0 * n * ball.vel.dot(n)
                ball.center = ball.center - n * ((ball.center - p2).dot(n) - ball.radius)
                self.tux.hit(ball, wall, ball.vel - vel, ball.center - center)