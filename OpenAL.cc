// This file is part of Ping Pong Team Arena.

// Ping Pong Team Arena is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Ping Pong Team Arena is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Ping Pong Team Arena.  If not, see <https://www.gnu.org/licenses/>.

#include <vorbis/vorbisfile.h>
#include <vorbis/codec.h>
#include <iostream>
#include <list>
#include <AL/al.h>
#include <AL/alc.h>
#include <sndfile.h>
#include <unistd.h>
#include "OpenALLib.hh"
#include <boost/python.hpp>
#include <Python.h>

using namespace boost::python;

BOOST_PYTHON_MODULE(OpenAL)

{
    class_< OpenAL >("OpenAL")
        .def("load", &OpenAL::load)
        .def("play", &OpenAL::play)
        .def("pause", &OpenAL::pause)
        .def("stop", &OpenAL::stop);
}
