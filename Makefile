all:	OpenALLib.so OpenAL.so

OpenALLib.so:	OpenALLib.cc
		g++ -fPIC -c OpenALLib.cc -std=gnu++17
		g++ -shared -o OpenALLib.so OpenALLib.o -lopenal -lsndfile
		
OpenALLib.o:	
		g++ -shared -o OpenALLib.o -lopenal -lsndfile

OpenAL.o:	OpenAL.cc
		g++ -fPIC -c OpenAL.cc -std=gnu++17 -I /usr/include/python3.9/

OpenAL.so:	OpenALLib.o OpenAL.o
		g++ -shared -o OpenAL.so OpenAL.o OpenALLib.o -lopenal -lsndfile -lboost_python39

